import asyncio

from twitch_ws_irc.client import TwitchWSIRCClient
from twitch_ws_irc.events import EventHandler, PrivMsgEvent


class BombsBot(EventHandler):
    client: TwitchWSIRCClient
    bombs: int

    def __init__(self, token, username, channel):
        self.client = TwitchWSIRCClient(token, username, channel)

    async def start(self):
        await self.client.start(self)
        await asyncio.start_server(lambda r, w: self.handler(r, w), port=10000)

    async def handler(self, reader: asyncio.StreamReader, writer: asyncio.StreamWriter):
        print('connected')
        try:
            while not reader.at_eof():
                self.bombs = int((await reader.readline()).decode().strip('\n'))
        except ConnectionResetError:
            pass
        print('disconnected')

    async def on_privmsg(self, event: PrivMsgEvent):
        if event.message.startswith('!bomb'):
            await event.reply(f'Streamer has {self.bombs} bombs!')


async def main():
    bot = BombsBot('token', 'username', 'channel')
    await bot.start()
    while True:
        await asyncio.sleep(1000)


if __name__ == '__main__':
    asyncio.run(main())

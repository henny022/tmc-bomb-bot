old_bombs = -1

local callback = function()
	new_bombs = memory.read_s8(0x2aec, "EWRAM")
	if (old_bombs ~= new_bombs) then
		comm.socketServerSend(string.format("%d\n", new_bombs))
		old_bombs = new_bombs
	end
end;
local mem_cb_id = event.onframeend(callback);
